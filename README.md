# Final Project Jarkom

ARLO MARIO DENDI
4210181018

DO OR DICE 

GAME OVERVIEW
Permainan realtime yang dapat dimainkan oleh beberapa client. Permainan ini memiliki faktor luck. Selain itu pemain dapat memenangkan permainan dengan cara mengatur strategi untuk membeli Golden Dice untuk memenangkan permainan.
Player yang awalnya memiliki 500 credit akan menggandakan creditnya dengan cara bertaruh dalam memilih warna. Tersedia 3 warna yang masing-masing mewakili jumlah dadu yang muncul. Jika player berhasil menebak warna , maka akan mendapatkan  multiplier
Jika player gagal, maka player akan kehilangan credit. Player yang kehabisan credit akan langsung kalah. Player harus mengumpulkan credit untuk membeli golden dice. Dadu akan diacak oleh server selama 3 detik untuk tiap player. Jadi, setiap player akan mendapatkan hasil jumlah dadu yang berbeda-beda.